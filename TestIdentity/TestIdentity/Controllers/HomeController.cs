﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using TestIdentity.Models;

namespace TestIdentity.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            dynamic list = null;
            if (User.Identity.IsAuthenticated)
            {

                //if (isAdminUser())
                //{
                //    //return RedirectToAction("Home", "Index");
                //}
            }
            List<IdentityRole> roles;
            List<ApplicationUser> users = new List<ApplicationUser>();
            using (ApplicationContext applicationContext = new ApplicationContext())
            {
                users = applicationContext.Users.ToList();
                 list = applicationContext.Roles.OrderBy(r => r.Name).ToList().Select(rr => new SelectListItem { Value = rr.Name.ToString(), Text = rr.Name }).ToList();
                roles = applicationContext.Roles.ToList();
            }

            var t = GetSelectListItems(users);
            ViewBag.Roles = list;
            ViewBag.Test = t;
            return View(users);
        }
        private bool isAdminUser()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ApplicationContext context = new ApplicationContext();
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var s = UserManager.GetRoles(user.GetUserId());
                if (s[0].ToString() == "Admin")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        private IEnumerable<SelectListItem> GetSelectListItems(IEnumerable<ApplicationUser> elements)
        {
            // Create an empty list to hold result of the operation
            var selectList = new List<SelectListItem>();

            // For each string in the 'elements' variable, create a new SelectListItem object
            // that has both its Value and Text properties set to a particular value.
            // This will result in MVC rendering each item as:
            //     <option value="State Name">State Name</option>

            foreach (var element in elements)
            {

                selectList.Add(new SelectListItem
                {
                    Value = element.UserName,
                    Text = element.UserName
                });
            }

            return selectList;
        }
    }
}