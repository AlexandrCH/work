﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestIdentity.Models
{
    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        /// Create a new instance of class <see cref="ApplicationUser"/>
        /// </summary>
        public ApplicationUser()
        {
        }
    }
}